// const container = document.querySelector("article");

// const tweetTexts = container.querySelectorAll(`[data-testid="tweetText"]`);
// const tweetTextContents = Array.from(tweetTexts).map(element => element.textContent);
// let prompt = "Generate a reply to this tweet:\n\n" + tweetTextContents.join("Replying to another tweet:\n\n");

const api_key = localStorage.getItem("api_key");
const org_id = localStorage.getItem("org_id");
if(api_key === null || org_id === null) {
    changeView("configuration");
} else {
    changeView("generate");
    checkPost();
}
document.getElementById("submit").addEventListener("click", function(e) {
    e.preventDefault();
    const api_key = document.getElementById("api_key").value;
    const org_id = document.getElementById("org_id").value;
    localStorage.setItem("api_key", api_key);
    localStorage.setItem("org_id", org_id);
    changeView("generate")
});

function changeView(viewName) {
    const elements = document.querySelectorAll("[data-view]");
    elements.forEach(element => {
        element.style.display = "none";
    });
    document.querySelector(`[data-view="${viewName}"]`).style.display = "block";
}

function checkPost() {
    const article = document.querySelector("article");
    console.log(article)
}